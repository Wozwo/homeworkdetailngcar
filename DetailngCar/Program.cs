﻿using System;

namespace DetailngCar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Реализация автомобиля.\n");
            ///Создание автомобиля
            Car car = new Car("Chevrolet Tahoe", new Body(), new Engine(), new WheelDrive());
            Console.WriteLine($"Марка и модель автомобиля: {car}\n");
            Console.WriteLine($"Объем топлива в баке до начала движения {car.GetDashboard()[3]}\n");
            ///Перемещение автомобиля
            car.Move(4);
            Console.WriteLine($"Пройденный путь автомобиля из точки A в точку B составил 4 км");
            car.Move(9);
            Console.WriteLine($"Пройденный путь автомобиля из точки B в точку C составил 9 км");
            ///Показания одометра
            Console.WriteLine($"Пробег: {car.CarMileage} км\n");
            ///Получение приборной панели автомобиля
            var dashboard = car.GetDashboard();
            ///Отображение данных приборной панели автомобиля
            Console.WriteLine($"Данные приборной панели:");
            for (int i = 0; i < 4; i++)
            {
                if (i == 0) Console.WriteLine($"Показания спидометра: {dashboard[i]} км/ч");
                else if (i == 1) Console.WriteLine($"Показания тахометра: {dashboard[i]} об/мин");
                else if (i == 2) Console.WriteLine($"Показания температуры ОЖ: {dashboard[i]} С");
                else if (i == 3) Console.WriteLine($"Показания объема топлива: {dashboard[i]} литорв");
            }
        }
    }
}
