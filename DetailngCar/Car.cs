﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DetailngCar
{
    public class Car
    {
        /// <summary>
        /// Класс Car описывает автомобиль, его конструктор принимает параметры:
        /// наименование марки - модели авто, кузов, двигатель и привод автомобиля.
        /// Так же создается экземпляр класса описывающего топливный бак.
        /// Для данного класа реализован метод расширения для передвижения авто в классе Actions.
        /// Так же здесь создается эекземпляр класса описывающий приборную панель автомобиля CarDashboard.
        /// Здесь переопределен метод ToString() для отображения марки - модели автомобиля.
        /// </summary>
        private readonly Body body;
        private readonly Engine engine;
        private readonly WheelDrive wheelDrive;
        private readonly string carModel;

        private CarDashboard dashboard = new CarDashboard();
        public FuelTank fuelTank;
        public Car (string carModel, Body body, Engine engine, WheelDrive wheelDrive)
        {
            this.carModel = carModel;
            this.body = body;
            this.engine = engine;
            this.wheelDrive = wheelDrive;

            fuelTank = new FuelTank(dashboard) + 10;
        }

        public int CarMileage { get; set; }
        
        public override string ToString()
        {
            return carModel;
        }

        public CarDashboard GetDashboard()
        {
            return dashboard;
        }
    }
}
