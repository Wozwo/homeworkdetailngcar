﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DetailngCar
{
    public class CarDashboard
    {
        /// <summary>
        /// В этом классе описана приборная панель автомобиля.
        /// Так же создан индексатор для класса CarDashboard, что бы получить данные с приборной панели по индексу.
        /// </summary>
        private double Speedometer { get; set; } = 55;
        private double Tachometer { get; set; } = 2870;
        private double CoolantTemperature { get; set; } = 90;
        private double FuelGauge { get; set; }

        public double this [int i]
        {
            get
            {
                switch (i)
                {
                    case 0: return Speedometer;
                    case 1: return Tachometer;
                    case 2: return CoolantTemperature;
                    case 3: return FuelGauge;
                    default:
                        throw new Exception();
                }
            }
            set
            {
                switch (i)
                {
                    case 0: Speedometer = value;
                        break;
                    case 1: Tachometer = value;
                        break;
                    case 2: CoolantTemperature = value;
                        break;
                    case 3: FuelGauge = value;
                        break;
                }
            }
        }
    }
}
