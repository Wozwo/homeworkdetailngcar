﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DetailngCar
{
    public class FuelTank
    {
        /// <summary>
        /// В этом классе описан топливный бак автомобиля.
        /// Так же реализован функционал изменения объема топлива в баке
        /// с помощью переопределения операторов сложения и вычитания.
        /// Конструктор данного класса принимает экземпляр класса приборной панели автомобиля
        /// для передачи данных из топливного бака об уровне топлива с помощью метода SendDashboardFuel().
        /// </summary>

        CarDashboard carDashboard = null;
        public FuelTank(CarDashboard carDashboard)
        {
            this.carDashboard = carDashboard;
        }
        public double Volume { get; } = 100;
        public double FuelLevel { get; set; }
        private void SendDashboardFuel()
        {
            carDashboard[3] = FuelLevel;
        }
        public static FuelTank operator + (FuelTank fuelTank, double refueling)
        {
            fuelTank.FuelLevel = fuelTank.FuelLevel + refueling;
            fuelTank.SendDashboardFuel();
            return fuelTank;
        }

        public static FuelTank operator - (FuelTank fuelTank, double refueling)
        {
            fuelTank.FuelLevel = fuelTank.FuelLevel - refueling;
            fuelTank.SendDashboardFuel();
            return fuelTank;
        }
    }
}
