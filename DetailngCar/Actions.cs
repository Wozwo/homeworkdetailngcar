﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DetailngCar
{
    public static class Actions
    {
        /// <summary>
        /// В этом классе реализован метод расширения для передвижения автомобиля.
        /// Используется оператор -= и += для изменения объема топлива в баке и изменения общего пробега автомобиля.
        /// </summary>
       
        public static void Move(this Car car, int distance)
        {
            car.fuelTank -= (0.1 * distance);
            car.CarMileage += distance;
        }
    }
}
